#pragma once

#include <vector>
#include "IGraph.h"

struct ArcGraph : public IGraph {
 private:
  std::vector<std::pair<int, int> > edges_vector;
  int vertices_count = 0;
 public:
  ArcGraph(int n): vertices_count(n) {} 
  ~ArcGraph() = default;
  ArcGraph(const IGraph *graph);

  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int>& vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int>& vertices) const override;
};
