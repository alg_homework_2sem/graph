#pragma once

#include <list>
#include <vector>
#include "IGraph.h"

struct ListGraph : public IGraph {
 private:
  std::vector<std::list<int> > adjacency_list;
  std::vector<std::list<int> > reverse_adjacency_list;

 public:
  ListGraph(int n) : adjacency_list(n), reverse_adjacency_list(n) {}
  ~ListGraph() = default;
  ListGraph(const IGraph *graph);
  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int>& vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int>& vertices) const override;
};
