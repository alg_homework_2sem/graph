#pragma once

#include "ListGraph.h"

using std::list;
using std::vector;

void ListGraph::AddEdge(int from, int to) {
  ListGraph::adjacency_list[from].push_front(to);
  ListGraph::reverse_adjacency_list[to].push_front(from);
}

int ListGraph::VerticesCount() const {
  return ListGraph::adjacency_list.size();
}

void ListGraph::GetNextVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ListGraph::adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

void ListGraph::GetPrevVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ListGraph::reverse_adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

ListGraph::ListGraph(const IGraph *graph)
    : adjacency_list(graph->VerticesCount()) {
  vector<int> temp;
  for (int i = 0; i < adjacency_list.size(); i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    adjacency_list[i].insert(adjacency_list[i].end(), temp.begin(), temp.end());
  }
}
