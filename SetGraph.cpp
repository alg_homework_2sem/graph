#pragma once

#include "SetGraph.h"

using std::unordered_set;
using std::vector;

void SetGraph::AddEdge(int from, int to) {
  SetGraph::adjacency_set[from].insert(to);
  SetGraph::reverse_adjacency_set[to].insert(from);
}

int SetGraph::VerticesCount() const { return SetGraph::adjacency_set.size(); }

void SetGraph::GetNextVertices(int vertex, vector<int>& vertices) const {
  for (auto it : SetGraph::adjacency_set[vertex]) {
    vertices.push_back(it);
  }
}

void SetGraph::GetPrevVertices(int vertex, vector<int>& vertices) const {
  for (auto it : SetGraph::reverse_adjacency_set[vertex]) {
    vertices.push_back(it);
  }
}

SetGraph::SetGraph(const IGraph* graph) : adjacency_set(graph->VerticesCount()) {
  vector<int> temp;
  for (int i = 0; i < adjacency_set.size(); i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    adjacency_set[i] = std::unordered_set<int>(temp.begin(), temp.end());
  }
}

