#pragma once
#include "MatrixGraph.h"

using std::vector;

void MatrixGraph::AddEdge(int from, int to) { adjacency_matrix[from][to] = 1; }

int MatrixGraph::VerticesCount() const {
  return MatrixGraph::adjacency_matrix.size();
}

void MatrixGraph::GetNextVertices(int vertex, vector<int>& vertices) const {
  for (int j = 0; j < adjacency_matrix.size(); j++) {
    if (adjacency_matrix[vertex][j] == 1) vertices.push_back(j);
  }
}

void MatrixGraph::GetPrevVertices(int vertex, vector<int>& vertices) const {
  for (int i = 0; i < adjacency_matrix.size(); i++) {
    if (adjacency_matrix[i][vertex]) vertices.push_back(i);
  }
}

MatrixGraph::MatrixGraph(const IGraph* graph)
    : adjacency_matrix(graph->VerticesCount(), vector<int>(graph->VerticesCount())) {
  vector<int> temp;
  for (int i = 0; i < adjacency_matrix.size(); i++) { 
    temp.clear();
    graph->GetNextVertices(i, temp);
    for (auto it : temp) adjacency_matrix[i][it] = 1;
  }
}
