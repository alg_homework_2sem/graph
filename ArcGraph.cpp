#pragma once

#include "ArcGraph.h"

using std::vector;

void ArcGraph::AddEdge(int from, int to) { edges_vector.push_back({from, to}); }

int ArcGraph::VerticesCount() const { return ArcGraph::vertices_count; }

void ArcGraph::GetNextVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ArcGraph::edges_vector) {
    if (it.first == vertex) vertices.push_back(it.second);
  }
}

void ArcGraph::GetPrevVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ArcGraph::edges_vector)
    if (it.second == vertex) vertices.push_back(it.first);
}


ArcGraph::ArcGraph(const IGraph* graph) : ArcGraph(graph->VerticesCount()) {
  vector<int> temp;
  for (int i = 0; i < vertices_count; i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    for (auto it : temp) edges_vector.push_back({i, it});
  }
}
