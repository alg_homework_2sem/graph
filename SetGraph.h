#pragma once

#include <unordered_set>
#include <vector>
#include "IGraph.h"

struct SetGraph : public IGraph {
 private:
  std::vector<std::unordered_set<int> > adjacency_set;
  std::vector<std::unordered_set<int> > reverse_adjacency_set;
 public:
  SetGraph(int n) : adjacency_set(n), reverse_adjacency_set(n) {}
  ~SetGraph() = default;
  SetGraph(const IGraph* graph);

  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int>& vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int>& vertices) const override;
};
