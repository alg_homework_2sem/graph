#pragma once

#include <vector>
#include "IGraph.h"

struct MatrixGraph : public IGraph {
 private:
  std::vector<std::vector<int> > adjacency_matrix;

 public:
  MatrixGraph(int n) : adjacency_matrix(n, std::vector<int>(n)) {}
  ~MatrixGraph() = default;
  MatrixGraph(const IGraph *graph);

  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int>& vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int>& vertices) const override;
};
